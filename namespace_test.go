package tahu

import (
	"strconv"
	"testing"
)

var globalt int
var globalString string

func makeNamespaceValue(groupID, messageType, nodeID, deviceID string) Namespace {
	return Namespace{
		Namespace:   "spBv1.0",
		GroupID:     groupID,
		MessageType: messageType,
		NodeID:      nodeID,
		DeviceID:    deviceID,
	}
}

func BenchmarkValueReturn(b *testing.B) {
	b.ReportAllocs()

	t := 0
	for i := 0; i < b.N; i++ {
		v := makeNamespaceValue("string"+strconv.FormatInt(int64(i), 10), "string2", "string3", "string4")
		t += len(v.GroupID)
	}
	globalt = t
}

func BenchmarkPointerReturn(b *testing.B) {
	b.ReportAllocs()

	t := 0
	for i := 0; i < b.N; i++ {
		v := MakeNamespace("string1"+strconv.FormatInt(int64(i), 10), "string2", "string3", "string4")
		t += len(v.GroupID)
	}
	globalt = t
}

func changeByValue(ns Namespace) Namespace {
	ns.GroupID = "Peter"
	return ns
}

func changeByPtr(ns *Namespace) *Namespace {
	ns.GroupID = "Peter"
	return ns
}

const bigStructSize = 10

type bigStruct struct {
	a [bigStructSize]int
}

func newBigStruct() bigStruct {
	var b bigStruct
	for i := 0; i < bigStructSize; i++ {
		b.a[i] = i
	}
	return b
}

func newBigStructPtr() *bigStruct {
	var b bigStruct
	for i := 0; i < bigStructSize; i++ {
		b.a[i] = i
	}
	return &b
}

func BenchmarkStructReturnValue(b *testing.B) {
	b.ReportAllocs()
	t := 0
	for i := 0; i < b.N; i++ {
		v := newBigStruct()
		t += v.a[0]
	}
}

func BenchmarkStructReturnPointer(b *testing.B) {
	b.ReportAllocs()
	t := 0
	for i := 0; i < b.N; i++ {
		v := newBigStructPtr()
		t += v.a[0]
	}
}

func BenchmarkPassByValue(b *testing.B) {
	b.ReportAllocs()
	ns := MakeNamespace("group", "message", "node", "device")
	for i := 0; i < b.N; i++ {
		ns = changeByValue(ns)
		globalString = ns.GroupID
	}
}

func BenchmarkPassByPtr(b *testing.B) {
	b.ReportAllocs()
	ns := MakeNamespace("group", "message", "node", "device")
	nsp := &ns
	for i := 0; i < b.N; i++ {
		nsp = changeByPtr(nsp)
		globalString = ns.GroupID
	}
}
