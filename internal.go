package tahu

import (
	fmt "fmt"
	"reflect"
	"strings"
	"time"
)

// Type Names
const (
	Unknown  = 0
	Int8     = 1
	Int16    = 2
	Int32    = 3
	Int64    = 4
	UInt8    = 5
	UInt16   = 6
	UInt32   = 7
	UInt64   = 8
	Float    = 9
	Double   = 10
	Boolean  = 11
	String   = 12
	DateTime = 13
	Text     = 14
	UUID     = 15
	DataSet  = 16
	Bytes    = 17
	File     = 18
	Template = 19
)

// makeSequence creates a sequence
func makeSequence() <-chan uint8 {
	c := make(chan uint8)
	var i uint8
	go func() {
		for {
			c <- i
			i++
		}
	}()
	return c
}

func (metric *Payload_Metric) addValue(value interface{}) error {
	switch x := value.(type) {
	case int8:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(Int8)
		metric.Datatype = &v
	case int16:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(Int16)
		metric.Datatype = &v
	case int32:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(Int32)
		metric.Datatype = &v
	case int:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(Int32)
		metric.Datatype = &v
	case int64:
		metric.Value = &Payload_Metric_LongValue{uint64(x)}
		v := uint32(Int64)
		metric.Datatype = &v
	case uint8:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(UInt8)
		metric.Datatype = &v
	case uint16:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(UInt16)
		metric.Datatype = &v
	case uint32:
		metric.Value = &Payload_Metric_IntValue{uint32(x)}
		v := uint32(UInt32)
		metric.Datatype = &v
	case uint64:
		metric.Value = &Payload_Metric_LongValue{uint64(x)}
		v := uint32(UInt64)
		metric.Datatype = &v
	case float32:
		metric.Value = &Payload_Metric_FloatValue{float32(x)}
		v := uint32(Float)
		metric.Datatype = &v
	case float64:
		metric.Value = &Payload_Metric_DoubleValue{x}
		v := uint32(Double)
		metric.Datatype = &v
	case bool:
		metric.Value = &Payload_Metric_BooleanValue{x}
		v := uint32(Boolean)
		metric.Datatype = &v
	case string:
		metric.Value = &Payload_Metric_StringValue{x}
		v := uint32(String)
		metric.Datatype = &v
	case time.Time:
		metric.Value = &Payload_Metric_LongValue{uint64(x.UnixNano() / 1000000)}
		v := uint32(DateTime)
		metric.Datatype = &v
	case time.Duration:
		metric.Value = &Payload_Metric_LongValue{uint64(x.Nanoseconds() / 1000000)}
		v := uint32(Int64)
		metric.Datatype = &v
	default:
		t := reflect.TypeOf(value)
		if t == nil {
			return nil
		}
		if t.Kind() != reflect.Struct {
			return fmt.Errorf("Metric of type %s is not implemented", reflect.TypeOf(value).Name())
		}
		v := reflect.ValueOf(value)
		pt := Payload_Template{}
		for i := 0; i < v.NumField(); i++ {
			pt.AddMetric(t.Field(i).Name, v.Field(i).Interface())
		}
		// Check whether this is a template definition
		if metric.Name != nil && strings.HasPrefix(*metric.Name, "_type_") {
			b := true
			pt.IsDefinition = &b
		} else {
			name := t.String()
			if t.Kind() == reflect.Ptr {
				name = t.Elem().String()
			}
			pt.TemplateRef = &name
		}
		metric.Value = &Payload_Metric_TemplateValue{&pt}
		dtv := uint32(Template)
		metric.Datatype = &dtv
	}
	return nil
}

func (metric *Payload_Template_Parameter) addValue(value interface{}) error {
	switch x := value.(type) {
	case int8:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(Int8)
		metric.Type = &v
	case int16:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(Int16)
		metric.Type = &v
	case int32:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(Int32)
		metric.Type = &v
	case int:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(Int32)
		metric.Type = &v
	case int64:
		metric.Value = &Payload_Template_Parameter_LongValue{uint64(x)}
		v := uint32(Int64)
		metric.Type = &v
	case uint8:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(UInt8)
		metric.Type = &v
	case uint16:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(UInt16)
		metric.Type = &v
	case uint32:
		metric.Value = &Payload_Template_Parameter_IntValue{uint32(x)}
		v := uint32(UInt32)
		metric.Type = &v
	case uint64:
		metric.Value = &Payload_Template_Parameter_LongValue{uint64(x)}
		v := uint32(UInt64)
		metric.Type = &v
	case float32:
		metric.Value = &Payload_Template_Parameter_FloatValue{float32(x)}
		v := uint32(Float)
		metric.Type = &v
	case float64:
		metric.Value = &Payload_Template_Parameter_DoubleValue{x}
		v := uint32(Double)
		metric.Type = &v
	case bool:
		metric.Value = &Payload_Template_Parameter_BooleanValue{x}
		v := uint32(Boolean)
		metric.Type = &v
	case string:
		metric.Value = &Payload_Template_Parameter_StringValue{x}
		v := uint32(String)
		metric.Type = &v
	case time.Time:
		metric.Value = &Payload_Template_Parameter_LongValue{uint64(x.UnixNano() / 1000000)}
		v := uint32(DateTime)
		metric.Type = &v
	case time.Duration:
		metric.Value = &Payload_Template_Parameter_LongValue{uint64(x.Nanoseconds() / 1000000)}
		v := uint32(Int64)
		metric.Type = &v
	default:
		return fmt.Errorf("Metric of type %s is not implemented", reflect.TypeOf(value).Name())
	}
	return nil
}
