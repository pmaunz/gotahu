package tahu

import "testing"

func TestPattern(t *testing.T) {
	type test struct {
		ns      Namespace
		pattern string
		want    bool
	}

	ns1 := makeNamespaceValue("groupID", "messageType", "nodeID", "deviceID")
	tests := []test{
		{ns: ns1, pattern: "#", want: true},
		{ns: ns1, pattern: "+/+/+/nodeID/+", want: true},
		{ns: ns1, pattern: "+/+/#", want: true},
		{ns: ns1, pattern: "+/groupID/+/#", want: true},
		{ns: ns1, pattern: "+/group/#", want: false},
		{ns: ns1, pattern: "+/+/+", want: false},
	}

	for _, tc := range tests {
		got, _ := tc.ns.Match(tc.pattern)
		if tc.want != got {
			t.Errorf("Namespace: %s, Pattern: %s, expected: %v, got: %v", tc.ns.Topic(), tc.pattern, tc.want, got)
		}
	}
	for _, tc := range tests {
		got, _ := TopicMatch(tc.ns.Topic(), tc.pattern)
		if tc.want != got {
			t.Errorf("Topic: %s, Pattern: %s, expected: %v, got: %v", tc.ns.Topic(), tc.pattern, tc.want, got)
		}
	}
}

func TestIsSubpattern(t *testing.T) {
	type test struct {
		subpattern string
		pattern    string
		want       bool
	}

	tests := []test{
		{subpattern: "hallo/welt", pattern: "#", want: true},
		{subpattern: "hallo/welt", pattern: "hallo/#", want: true},
		{subpattern: "hallo/welt", pattern: "hallo/+", want: true},
		{subpattern: "hallo/welt", pattern: "+/welt", want: true},
		{subpattern: "hallo/welt", pattern: "+/+/#", want: true},
		{subpattern: "#", pattern: "#", want: true},
		{subpattern: "+", pattern: "#", want: true},
		{subpattern: "#", pattern: "hallo/+", want: false},
		{subpattern: "+/+", pattern: "+/welt", want: false},
		{subpattern: "+/#", pattern: "+/+/#", want: false},
	}

	for _, tc := range tests {
		got := IsSubpattern(tc.subpattern, tc.pattern)
		if tc.want != got {
			t.Errorf("Subpattern: %s, Pattern: %s, expected: %v, got: %v", tc.subpattern, tc.pattern, tc.want, got)
		}
	}
}
