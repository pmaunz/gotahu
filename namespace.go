package tahu

import (
	fmt "fmt"
	"regexp"
	"strings"
)

// SparkplugNamespace is the standard namespace for sparkplug
const (
	SparkplugNamespace = "spBv1.0"
)

// Namespace encapsulates the topic namespace
type Namespace struct {
	Namespace   string
	GroupID     string
	MessageType string
	NodeID      string
	DeviceID    string
}

// ParseNamespace parses Namespace from Topic
func ParseNamespace(topic string) (Namespace, error) {
	elems := strings.Split(topic, "/")
	if len(elems) < 4 {
		return Namespace{}, fmt.Errorf("Topic '%s' does not adhere to Sparkplug specification, wrong number of elements", topic)
	}
	ns := Namespace{
		Namespace:   elems[0],
		GroupID:     elems[1],
		MessageType: elems[2],
		NodeID:      elems[3],
	}
	if len(elems) == 5 {
		ns.DeviceID = elems[4]
	}
	return ns, nil
}

// ParseAbsID createsNamespace from AbsNodeID or AbsDeviceID
func ParseAbsID(nodeID string) (*Namespace, error) {
	elems := strings.Split(nodeID, "/")
	ns := Namespace{
		Namespace: "spBv1.0",
	}
	if len(elems) < 2 {
		return nil, fmt.Errorf("ID '%s' must have at least 2 elements", nodeID)
	}
	ns.GroupID = elems[0]
	ns.NodeID = elems[1]
	if len(elems) == 3 {
		ns.DeviceID = elems[2]
	}
	return &ns, nil
}

// MakeNamespace creates empty Namespace
func MakeNamespace(groupID, messageType, nodeID, deviceID string) Namespace {
	return Namespace{
		Namespace:   SparkplugNamespace,
		GroupID:     groupID,
		MessageType: messageType,
		NodeID:      nodeID,
		DeviceID:    deviceID,
	}
}

var escapeExp = regexp.MustCompile(`[.*+?^${}()|[\]\\]`)

// RegexpFromTopic converts a topic with + wildcards into a regular expression
func RegexpFromTopic(s string) (*regexp.Regexp, error) {
	s = escapeExp.ReplaceAllString(s, "\\$0")
	s = strings.ReplaceAll(s, `\+`, `[^/]+`)
	s = strings.ReplaceAll(s, "#", `.*`)
	return regexp.Compile("^" + s + "$")
}

// Match returns true if the namespace matches the pattern
func (n *Namespace) Match(pattern string) (bool, error) {
	exp, err := RegexpFromTopic(pattern)
	if err != nil {
		return false, fmt.Errorf("Cannot compile regular expression for '%v': %v", pattern, err)
	}
	return exp.MatchString(n.Topic()), nil
}

// TopicMatch returns true if the namespace matches the pattern
func TopicMatch(topic string, pattern string) (bool, error) {
	exp, err := RegexpFromTopic(pattern)
	if err != nil {
		return false, fmt.Errorf("Cannot compile regular expression for '%v': %v", pattern, err)
	}
	return exp.MatchString(topic), nil
}

func min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

// IsSubpattern returns true iff pattern always matches if subpattern matches
// that is the set of topics that pattern matches includes the set that subpattern matches
func IsSubpattern(subpattern string, pattern string) bool {
	sub := strings.Split(subpattern, "/")
	pat := strings.Split(pattern, "/")
	if len(pat) > len(sub) { // this means pattern is more specific
		if sub[len(sub)-1] == "#" {
			return false
		}
		if pat[len(pat)-1] == "#" && len(pat) == len(sub)+1 { // patterns ends in # and is 1 element longer
			return true
		}
		return false
	}
	minlen := min(len(sub), len(pat))
	for i := 0; i < minlen; i++ {
		if pat[i] == "#" {
			return true
		}
		if sub[i] != pat[i] && pat[i] != "+" {
			return false
		}
	}
	if len(sub) > len(pat) {
		return false
	}
	return true
}

// EqualEndpoint returns true if the Namespace, Group, Node and Device match
func (n *Namespace) EqualEndpoint(o Namespace) bool {
	return n.Namespace == o.Namespace && n.GroupID == o.GroupID && n.NodeID == o.NodeID && n.DeviceID == o.DeviceID
}

// EqualNode returns true if the Namespace, Group, Node and Device match
func (n *Namespace) EqualNode(o Namespace) bool {
	return n.Namespace == o.Namespace && n.GroupID == o.GroupID && n.NodeID == o.NodeID
}

// Topic returns MQTT topic from Namespace
func (n *Namespace) Topic() string {
	if n.DeviceID == "" {
		return n.Namespace + "/" + n.GroupID + "/" + n.MessageType + "/" + n.NodeID
	}
	return n.Namespace + "/" + n.GroupID + "/" + n.MessageType + "/" + n.NodeID + "/" + n.DeviceID
}

// MakeTopic from the parts
func MakeTopic(groupID, messageType, nodeID, deviceID string) string {
	if deviceID == "" {
		return SparkplugNamespace + "/" + groupID + "/" + messageType + "/" + nodeID
	}
	return SparkplugNamespace + "/" + groupID + "/" + messageType + "/" + nodeID + "/" + deviceID
}

// MakeNodeTopic from the parts
func MakeNodeTopic(groupID, messageType, nodeID string) string {
	return SparkplugNamespace + "/" + groupID + "/" + messageType + "/" + nodeID
}

// NodeTopic returns MQTT node Topic
func (n *Namespace) NodeTopic() string {
	return n.Namespace + "/" + n.GroupID + "/" + n.MessageType + "/" + n.NodeID
}

// AbsDeviceID returns "groupID/nodeID/deviceID"
func (n *Namespace) AbsDeviceID() string {
	return n.GroupID + "/" + n.NodeID + "/" + n.DeviceID
}

// AbsNodeID returns "groupID/nodeID"
func (n *Namespace) AbsNodeID() string {
	return n.GroupID + "/" + n.NodeID
}

// SetAbsNodeID sets groupID and nodeID from "groupID/nodeID"
func (n *Namespace) SetAbsNodeID(id string) error {
	elems := strings.Split(id, "/")
	if len(elems) != 2 {
		return fmt.Errorf("Node id '%s' does not adhere to specification, need 3 elements", id)
	}
	n.GroupID = elems[0]
	n.NodeID = elems[1]
	return nil
}

// SetAbsDeviceID sets groupID, nodeID, deviceID from "groupID/nodeID/deviceID"
func (n *Namespace) SetAbsDeviceID(id string) error {
	elems := strings.Split(id, "/")
	if len(elems) != 3 {
		return fmt.Errorf("Device id '%s' does not adhere to specification, need 4 elements", id)
	}
	n.GroupID = elems[0]
	n.NodeID = elems[1]
	n.DeviceID = elems[2]
	return nil
}
