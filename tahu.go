package tahu

import (
	fmt "fmt"
	"reflect"
	"sync"
	"time"

	"gitlab.sandia.gov/iontraps/bimap"

	MQTT "github.com/eclipse/paho.mqtt.golang"

	"github.com/golang/protobuf/proto"
)

// Sparkplug struct
type Sparkplug struct {
	SeqChan     <-chan uint8
	aliasMap    map[string]*AliasMap // Stores aliasmap with key of AbsoluteDeviceID
	aliasMapMux sync.Mutex
}

// AliasMap keeps map of aliases
type AliasMap struct {
	m *bimap.BiMap
}

// MakeAliasMap creates an aliasmap
func MakeAliasMap() *AliasMap {
	return &AliasMap{m: bimap.NewBiMap()}
}

// Expand expands Payload Names from Aliases
func (am *AliasMap) Expand(p *Payload) {
	for _, m := range p.Metrics {
		if m.Alias != nil {
			if m.Name == nil {
				n, found := am.m.GetInverse(*m.Alias)
				if found {
					m.Name = &n
				}
			} else {
				am.m.Insert(*m.Name, *m.Alias)
			}
		}
	}
}

// Create aliases for metrics
func (am *AliasMap) Create(p *Payload) {
	for _, m := range p.Metrics {
		if m.Name != nil && *m.Name != "" {
			if am.m.Exists(*m.Name) {
				v, _ := am.m.Get(*m.Name)
				m.Alias = &v
			} else {
				thisalias := uint64(am.m.Size() + 1)
				am.m.Insert(*m.Name, thisalias)
				m.Alias = &thisalias
			}
		}
	}
}

// Replace Name with Aliases
func (am *AliasMap) Replace(p *Payload) {
	for _, m := range p.Metrics {
		if m.Name != nil && *m.Name != "" && *m.Name != "bdSeq" {
			v, found := am.m.Get(*m.Name)
			if found {
				m.Alias = &v
				m.Name = nil
			}
		}
	}
}

// MakeSparkplug creates Sparkplug
func MakeSparkplug() *Sparkplug {
	seqchan := makeSequence()
	return &Sparkplug{SeqChan: seqchan, aliasMap: make(map[string]*AliasMap)}
}

// MakePayload creates a payload for sending with sequence and timestamp populated
func (s *Sparkplug) MakePayload() *Payload {
	seq := uint64(<-s.SeqChan)
	t := uint64(time.Now().UnixNano() / 1000)
	return &Payload{Seq: &seq, Timestamp: &t}
}

// Publish dispateches the message
func (s *Sparkplug) Publish(client MQTT.Client, ns Namespace, payload *Payload, retain bool) error {
	s.aliasMapMux.Lock()
	defer s.aliasMapMux.Unlock()
	am, found := s.aliasMap[ns.AbsDeviceID()]
	if !found {
		s.aliasMap[ns.AbsDeviceID()] = MakeAliasMap()
		am, _ = s.aliasMap[ns.AbsDeviceID()]
	}
	// Create Aliases for birth certificates
	if ns.MessageType == "NBIRTH" || ns.MessageType == "DBIRTH" {
		am.Create(payload)
	} else { // else replace with Aliases
		am.Replace(payload)
	}
	bytearray, _ := proto.Marshal(payload)
	token := client.Publish(ns.Topic(), 0, retain, bytearray)
	token.Wait()
	return token.Error()
}

// UnmarshalPayload and expand aliases
func (s *Sparkplug) UnmarshalPayload(ns Namespace, data []byte) (*Payload, error) {
	payload := &Payload{}
	err := proto.Unmarshal(data, payload)
	if err != nil {
		return payload, err
	}
	// Expand Aliases
	s.aliasMapMux.Lock()
	defer s.aliasMapMux.Unlock()
	am, found := s.aliasMap[ns.AbsDeviceID()]
	if !found {
		s.aliasMap[ns.AbsDeviceID()] = MakeAliasMap()
		am, _ = s.aliasMap[ns.AbsDeviceID()]
	}
	am.Expand(payload)
	return payload, err
}

// GetNodeLifePayloads Always request this before requesting the Node Birth Payload
func (s *Sparkplug) GetNodeLifePayloads(bdSeq uint8) (*Payload, *Payload) {
	// Death
	death := &Payload{}
	death.AddMetric("bdSeq", bdSeq)

	// Birth
	birth := s.MakePayload()
	birth.AddMetric("bdSeq", bdSeq)
	return birth, death
}

// GetDeviceBirthPayload get the DBIRTH payload
func (s *Sparkplug) GetDeviceBirthPayload(bdSeq uint8) *Payload {
	payload := s.MakePayload()
	payload.AddMetric("bdSeq", bdSeq)
	return payload
}

// GetDdataPayload Get a DDATA payload
func (s *Sparkplug) GetDdataPayload() *Payload {
	payload := s.MakePayload()
	return payload
}

// InitDatasetMetric Helper method for adding dataset metrics to a payload
func (p *Payload) InitDatasetMetric(name string, alias uint64, columns []string, datatypes []uint32) *Payload_DataSet {
	t := uint64(time.Now().UnixNano() / 1000)
	dt := uint32(DataSet)
	noc := uint64(len(datatypes))
	pt := Payload_DataSet{
		NumOfColumns: &noc,
		Columns:      columns,
		Types:        datatypes,
	}
	v := Payload_Metric_DatasetValue{&pt}
	metric := &Payload_Metric{
		Name:      &name,
		Alias:     &alias,
		Timestamp: &t, // ms since epoch
		Datatype:  &dt,
		Value:     &v,
	}
	// Set up the dataset
	p.Metrics = append(p.Metrics, metric)
	return &pt
}

// AddRow row to dataset
func (s *Payload_DataSet) AddRow(value []interface{}) {
	// TODO: Needs to be implemented
}

// InitTemplateMetric Helper method for adding dataset metrics to a payload
func (p *Payload) InitTemplateMetric(name string, alias uint64, templateRef string) *Payload_Template {
	t := uint64(time.Now().UnixNano() / 1000)
	temp := uint32(Template)
	id := templateRef == ""
	pt := Payload_Template{
		TemplateRef:  &templateRef,
		IsDefinition: &id,
	}
	v := Payload_Metric_TemplateValue{&pt}
	metric := &Payload_Metric{
		Name:      &name,
		Alias:     &alias,
		Timestamp: &t, // ms since epoch
		Datatype:  &temp,
		Value:     &v,
	}
	p.Metrics = append(p.Metrics, metric)
	return &pt
}

// AddParameter adds
func (t *Payload_Template) AddParameter(name string, value interface{}) error {
	ptp := Payload_Template_Parameter{
		Name: &name,
	}
	err := ptp.addValue(value)
	if err != nil {
		return err
	}
	t.Parameters = append(t.Parameters, &ptp)
	return nil
}

// AddMetric Helper method for adding metrics to a payload container
func (p *Payload) AddMetric(name string, value interface{}) (*Payload_Metric, error) {
	return p.AddMetricProperties(name, value, nil, false, false, false)
}

// AddMetricProperties Helper method for adding metrics to a payload container
func (p *Payload) AddMetricTimestamp(name string, value interface{}, timestamp time.Time, isNull bool, isTransient bool, isHistorical bool) (*Payload_Metric, error) {
	t := uint64(timestamp.UnixNano() / 1000)
	metric := &Payload_Metric{
		Name:         &name,
		Timestamp:    &t, // ms since epoch
		IsHistorical: &isHistorical,
		IsTransient:  &isTransient,
		IsNull:       &isNull,
	}
	err := metric.addValue(value)
	if err != nil {
		return nil, err
	}
	// Return the metric
	p.Metrics = append(p.Metrics, metric)
	return metric, nil
}

// AddMetricProperties Helper method for adding metrics to a payload container
func (p *Payload) AddMetricProperties(name string, value interface{}, properties map[string]interface{}, isNull bool, isTransient bool, isHistorical bool) (*Payload_Metric, error) {
	t := uint64(time.Now().UnixNano() / 1000)
	metric := &Payload_Metric{
		Name:         &name,
		Timestamp:    &t, // ms since epoch
		IsHistorical: &isHistorical,
		IsTransient:  &isTransient,
		IsNull:       &isNull,
	}
	err := metric.addValue(value)
	if err != nil {
		return nil, err
	}
	if properties != nil {
		metric.AddProperties(properties)
	}
	// Return the metric
	p.Metrics = append(p.Metrics, metric)
	return metric, nil
}

func makePayloadMetric(name string, timestamp time.Time, isHistorical bool, isTransient bool, isNull bool, value interface{}) (*Payload_Metric, error) {
	var t uint64
	if timestamp.IsZero() {
		t = uint64(time.Now().UnixNano() / 1000)
	} else {
		t = uint64(timestamp.UnixNano() / 1000)
	}
	metric := &Payload_Metric{
		Name:         &name,
		Timestamp:    &t,
		IsHistorical: &isHistorical,
		IsTransient:  &isTransient,
		IsNull:       &isNull,
	}
	err := metric.addValue(value)
	return metric, err
}

// AddMetrics adds all metrics provided as slice of Metric to the payload
func (p *Payload) AddMetrics(metrics []Metric) error {
	if metrics == nil {
		return nil
	}
	for _, m := range metrics {
		metric, _ := makePayloadMetric(m.Name, m.Timestamp, m.IsHistorical, m.IsTransient, m.IsNull, m.Value)
		metric.AddProperties(m.Properties)
		p.Metrics = append(p.Metrics, metric)
	}
	return nil
}

// ConvertMetrics converts metrics to slice of Metric
func (p *Payload) ConvertMetrics() ([]Metric, error) {
	metric := make([]Metric, 0)
	for _, pm := range p.GetMetrics() {
		m := Metric{
			Timestamp:    Time(pm.Timestamp),
			Value:        pm.ToInterfaceValue(),
			Name:         pm.GetName(),
			IsTransient:  pm.GetIsTransient(),
			IsHistorical: pm.GetIsHistorical(),
			IsNull:       pm.GetIsNull(),
			Properties:   pm.GetPropertiesMap(),
		}
		metric = append(metric, m)
	}
	return metric, nil
}

// AddValue adds value to Payload_PropertyValue
func (p *Payload_PropertyValue) AddValue(value interface{}) error {
	switch x := value.(type) {
	case int8:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(Int8)
		p.Type = &v
	case int16:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(Int16)
		p.Type = &v
	case int32:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(Int32)
		p.Type = &v
	case int:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(Int32)
		p.Type = &v
	case int64:
		p.Value = &Payload_PropertyValue_LongValue{uint64(x)}
		v := uint32(Int64)
		p.Type = &v
	case uint8:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(UInt8)
		p.Type = &v
	case uint16:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(UInt16)
		p.Type = &v
	case uint32:
		p.Value = &Payload_PropertyValue_IntValue{uint32(x)}
		v := uint32(UInt32)
		p.Type = &v
	case uint64:
		p.Value = &Payload_PropertyValue_LongValue{uint64(x)}
		v := uint32(UInt64)
		p.Type = &v
	case float32:
		p.Value = &Payload_PropertyValue_FloatValue{float32(x)}
		v := uint32(Float)
		p.Type = &v
	case float64:
		p.Value = &Payload_PropertyValue_DoubleValue{x}
		v := uint32(Double)
		p.Type = &v
	case bool:
		p.Value = &Payload_PropertyValue_BooleanValue{x}
		v := uint32(Boolean)
		p.Type = &v
	case string:
		p.Value = &Payload_PropertyValue_StringValue{x}
		v := uint32(String)
		p.Type = &v
	case time.Time:
		p.Value = &Payload_PropertyValue_LongValue{uint64(x.UnixNano() / 1000000)}
		v := uint32(DateTime)
		p.Type = &v
	case time.Duration:
		p.Value = &Payload_PropertyValue_LongValue{uint64(x.Nanoseconds() / 1000000)}
		v := uint32(Int64)
		p.Type = &v
	default:
		return fmt.Errorf("Metric of type %s is not implemented", reflect.TypeOf(value).Name())
	}
	return nil
}

// AddProperties adds the properties given as a map to the metric
func (m *Payload_Metric) AddProperties(properties map[string]interface{}) error {
	if m.Properties == nil {
		m.Properties = &Payload_PropertySet{}
	}
	props := m.Properties
	for key, value := range properties {
		props.Keys = append(props.Keys, key)
		v := Payload_PropertyValue{}
		err := v.AddValue(value)
		props.Values = append(props.Values, &v)
		if err != nil {
			return err
		}
	}
	return nil
}

// GetPropertiesMap gets the properties from a metric
func (m *Payload_Metric) GetPropertiesMap() map[string]interface{} {
	properties := m.GetProperties()
	propertiesMap := make(map[string]interface{})
	if properties == nil {
		return propertiesMap
	}
	for i := 0; i < len(properties.Keys); i++ {
		propertiesMap[properties.Keys[i]] = properties.Values[i].ToInterfaceValue()
	}
	return propertiesMap
}

// MakePropertySet creates a propertySet and populates it from the provided map
func MakePropertySet(properties map[string]interface{}) (*Payload_PropertySet, error) {
	props := &Payload_PropertySet{}
	for key, value := range properties {
		props.Keys = append(props.Keys, key)
		v := Payload_PropertyValue{}
		err := v.AddValue(value)
		if err != nil {
			return props, err
		}
		props.Values = append(props.Values, &v)
	}
	return props, nil
}

// AddMetric Helper method for adding metrics to a payload container
func (t *Payload_Template) AddMetric(name string, value interface{}) (*Payload_Metric, error) {
	ti := uint64(time.Now().UnixNano() / 1000)
	metric := &Payload_Metric{
		Name:      &name,
		Timestamp: &ti, // ms since epoch
	}
	err := metric.addValue(value)
	if err != nil {
		return metric, nil
	}
	// Return the metric
	t.Metrics = append(t.Metrics, metric)
	return metric, nil
}

// AddNullMetric Helper method for adding metrics to a payload
func (p *Payload) AddNullMetric(name string, datatype uint32) (*Payload_Metric, error) {
	t := uint64(time.Now().UnixNano() / 1000)
	b := true
	metric := &Payload_Metric{
		Name:      &name,
		Timestamp: &t, // ms since epoch
		IsNull:    &b,
		Datatype:  &datatype,
	}
	p.Metrics = append(p.Metrics, metric)
	return metric, nil
}

// BdSeq returns birth death sequence number or error if not found
func (p *Payload) BdSeq() (uint8, error) {
	for _, metric := range p.Metrics {
		if metric.Name != nil && *metric.Name == "bdSeq" {
			v, ok := metric.ToInterfaceValue().(uint8)
			if ok {
				return v, nil
			}
		}
	}
	return 0, fmt.Errorf("No valid bdSeq found")
}
