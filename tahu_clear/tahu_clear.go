package main

import (
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"regexp"
	"time"
	"unicode"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/juju/errors"
	tahu "gitlab.sandia.gov/iontraps/gotahu"
)

func makeOnConnect(sp *tahu.Sparkplug, onMessage MQTT.MessageHandler, topic string) MQTT.OnConnectHandler {
	// onConnect The callback for when the client receives a CONNACK response from the server.
	return func(client MQTT.Client) {
		log.Println("Connected")
		// Subscribing in onconnect() means that if we lose the connection and
		// reconnect then subscriptions will be renewed.
		client.Subscribe(topic, 0, onMessage)
	}
}

func filterMetric(ns tahu.Namespace, metrics []*tahu.Payload_Metric, filter messageFilters) bool {
	if !filter.nodeIDRe.MatchString(ns.NodeID) {
		return false
	}
	if !filter.deviceIDRe.MatchString(ns.DeviceID) {
		return false
	}
	for _, m := range metrics {
		if filter.metricRe.MatchString(m.GetName()) {
			return true
		}
	}
	return false
}

func makeOnMessage(sp *tahu.Sparkplug, filters messageFilters, doclear bool) (MQTT.MessageHandler, error) {
	err := filters.Compile()
	if err != nil {
		return nil, err
	}
	// onMessage The callback for when a PUBLISH message is received from the server
	return func(client MQTT.Client, msg MQTT.Message) {
		if !msg.Retained() {
			return
		}
		ns, err := tahu.ParseNamespace(msg.Topic())
		payload, err := sp.UnmarshalPayload(ns, msg.Payload())
		if err != nil {
			return
		}
		if !filterMetric(ns, payload.Metrics, filters) {
			return
		}
		log.Printf("%s\n", msg.Topic())
		if doclear {
			empty := make([]byte, 0)
			client.Publish(msg.Topic(), 0, true, empty)
		}
	}, nil
}

// IsPrintable returns true if the supplied string is printable
func IsPrintable(s string) bool {
	for _, r := range s {
		if !unicode.IsPrint(r) {
			return false
		}
	}
	return true
}

func onConnectionLost(client MQTT.Client, err error) {
	log.Printf("Connection Lost: %v\n", err)
}

type messageFilters struct {
	deviceID     string
	nodeID       string
	metric       string
	compact      bool
	deviceIDRe   *regexp.Regexp
	metricRe     *regexp.Regexp
	nodeIDRe     *regexp.Regexp
	maxAgeString string
	maxAge       time.Duration
}

func (mf *messageFilters) Compile() error {
	var err error
	mf.metricRe, err = regexp.Compile(mf.metric)
	if err != nil {
		return errors.Annotate(err, "Compiling Metric Regexp")
	}
	mf.deviceIDRe, err = regexp.Compile(mf.deviceID)
	if err != nil {
		return errors.Annotate(err, "Compiling Device Regexp")
	}
	mf.nodeIDRe, err = regexp.Compile(mf.nodeID)
	if err != nil {
		return errors.Annotate(err, "Compiling Node Regexp")
	}
	if mf.maxAgeString == "" {
		mf.maxAge = 100 * 365 * 24 * time.Hour
	} else {
		mf.maxAge, err = time.ParseDuration(mf.maxAgeString)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {
	filters := messageFilters{}

	// Commandline options
	host := flag.String("host", "localhost", "sever to connect to")
	port := flag.Int64("port", 0, "server port")
	proto := flag.String("proto", "", "Protocol ssl or tcp")
	caFile := flag.String("cafile", "", "ca.crt file path")
	keyFile := flag.String("key", "", "client authentication key file path")
	certFile := flag.String("cert", "", "client authentication certificate file path")
	topic := flag.String("topic", "#", "Topic to subscribe to")
	flag.StringVar(&filters.metric, "metric", "", "Metric Filter regexp")
	flag.BoolVar(&filters.compact, "compact", false, "Use compact display")
	flag.StringVar(&filters.deviceID, "device", "", "DeviceID filter regexp")
	flag.StringVar(&filters.nodeID, "node", "", "NodeID filter regexp")
	flag.StringVar(&filters.maxAgeString, "maxage", "", "Maximum age, older retained will be cleared")
	doclear := flag.Bool("doclear", false, "Only print retained messages to be cleared")
	flag.Parse()

	if *proto == "" {
		if *caFile != "" && *keyFile != "" && *certFile != "" {
			*proto = "ssl"
			if *port == 0 {
				*port = 8883
			}
		} else {
			*proto = "tcp"
			if *port == 0 {
				*port = 1883
			}
		}
	}
	log.Printf("Connecting to %s://%s:%d\n", *proto, *host, *port)

	// Stop on Ctrl-C
	log.Println("Starting main application")
	log.Printf("topic: '%s'\n", *topic)
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	tlsConfig := &tls.Config{}

	if *proto == "ssl" {
		// Prepare client authentication certificates
		cert, err := tls.LoadX509KeyPair(*certFile, *keyFile)
		if err != nil {
			log.Fatalf("Cannot load certificate and key files '%s' and '%s': %v\n", *certFile, *keyFile, err)
		}

		// Load CA cert
		caCert, err := ioutil.ReadFile(*caFile)
		if err != nil {
			log.Fatalf("Cannot load ca certificate '%s': %s\n", *caFile, err)
		}
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		// Setup HTTPS client
		tlsConfig = &tls.Config{
			Certificates: []tls.Certificate{cert},
			RootCAs:      caCertPool,
		}
		tlsConfig.BuildNameToCertificate()
	}
	// Prepare MQTT
	sparkplug := tahu.MakeSparkplug()
	onMessage, err := makeOnMessage(sparkplug, filters, *doclear)
	if err != nil {
		log.Fatalf("Filters: %+v, Error: %v\n", filters, err)
	}
	onConnect := makeOnConnect(sparkplug, onMessage, *topic)

	// Start of main program - Set up the MQTT client connection
	url := fmt.Sprintf("%s://%s:%d", *proto, *host, *port)
	opts := MQTT.NewClientOptions().AddBroker(url)
	opts.SetClientID(fmt.Sprintf("tahu_client_clear_%d", os.Getpid()))
	if *proto == "ssl" {
		opts.SetTLSConfig(tlsConfig)
	}
	opts.SetDefaultPublishHandler(onMessage)
	opts.SetOnConnectHandler(onConnect)
	opts.SetConnectionLostHandler(onConnectionLost)

	client := MQTT.NewClient(opts)
	token := client.Connect()
	token.Wait()

	if token.Error() != nil {
		log.Fatalf("MQTT cannot connect to '%s': %v\n", url, token.Error())
	}
	time.Sleep(2 * time.Second)
	//<-quit
}
