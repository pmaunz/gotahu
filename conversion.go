package tahu

import (
	fmt "fmt"
	"reflect"
	"time"
)

// Time returns time.Time from Timestamp
func Time(timestamp *uint64) time.Time {
	if timestamp == nil {
		return time.Unix(0, 0)
	}
	return time.Unix(int64(*timestamp/1000), int64((*timestamp%1000)*1000000))
}

// Timestamp covert time.Time to ms timestamp
func Timestamp(t time.Time) *uint64 {
	timestamp := uint64(t.UnixNano() / 1000000)
	return &timestamp
}

// ToStringValue converts a Value to String
func (m *Payload_Metric) ToStringValue() string {
	value := m.GetValue()
	datatype := m.GetDatatype()
	switch datatype {
	case Int8:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", int8(x.IntValue))
	case Int16:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", int16(x.IntValue))
	case Int32:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", int32(x.IntValue))
	case Int64:
		x, ok := value.(*Payload_Metric_LongValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", int64(x.LongValue))
	case UInt8:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", uint8(x.IntValue))
	case UInt16:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", uint16(x.IntValue))
	case UInt32:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", uint32(x.IntValue))
	case UInt64:
		x, ok := value.(*Payload_Metric_LongValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%d", uint64(x.LongValue))
	case Float:
		x, ok := value.(*Payload_Metric_FloatValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%v", float32(x.FloatValue))
	case Double:
		x, ok := value.(*Payload_Metric_DoubleValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%v", float64(x.DoubleValue))
	case String, UUID, Text:
		x, ok := value.(*Payload_Metric_StringValue)
		if !ok {
			return ""
		}
		return string(x.StringValue)
	case Boolean:
		x, ok := value.(*Payload_Metric_BooleanValue)
		if !ok {
			return ""
		}
		return fmt.Sprintf("%v", x.BooleanValue)
	case DateTime:
		x, ok := value.(*Payload_Metric_LongValue)
		if !ok {
			return ""
		}
		return time.Unix(int64(x.LongValue/1000), int64((x.LongValue%1000)*1000000)).String()
	}
	return ""
}

// ToInterfaceValue converts a Value to an interface{}
func (m *Payload_Metric) ToInterfaceValue() interface{} {
	value := m.GetValue()
	datatype := m.GetDatatype()
	switch datatype {
	case Int8:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return 0
		}
		return int8(x.IntValue)
	case Int16:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return 0
		}
		return int16(x.IntValue)
	case Int32:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return 0
		}
		return int32(x.IntValue)
	case Int64:
		x, ok := value.(*Payload_Metric_LongValue)
		if !ok {
			return 0
		}
		return int64(x.LongValue)
	case UInt8:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return 0
		}
		return uint8(x.IntValue)
	case UInt16:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return 0
		}
		return uint16(x.IntValue)
	case UInt32:
		x, ok := value.(*Payload_Metric_IntValue)
		if !ok {
			return 0
		}
		return uint32(x.IntValue)
	case UInt64:
		x, ok := value.(*Payload_Metric_LongValue)
		if !ok {
			return 0
		}
		return uint64(x.LongValue)
	case Float:
		x, ok := value.(*Payload_Metric_FloatValue)
		if !ok {
			return 0
		}
		return float32(x.FloatValue)
	case Double:
		x, ok := value.(*Payload_Metric_DoubleValue)
		if !ok {
			return 0
		}
		return float64(x.DoubleValue)
	case String, UUID, Text:
		x, ok := value.(*Payload_Metric_StringValue)
		if !ok {
			return 0
		}
		return string(x.StringValue)
	case Boolean:
		x, ok := value.(*Payload_Metric_BooleanValue)
		if !ok {
			return 0
		}
		return bool(x.BooleanValue)
	case DateTime:
		x, ok := value.(*Payload_Metric_LongValue)
		if !ok {
			time.Unix(0, 0)
		}
		return time.Unix(int64(x.LongValue/1000), int64((x.LongValue%1000)*1000000))
	}
	return 0
}

// ToInterfaceValue converts a Value to an interface{}
func (m *Payload_PropertyValue) ToInterfaceValue() interface{} {
	value := m.GetValue()
	datatype := m.GetType()
	switch datatype {
	case Int8:
		x, ok := value.(*Payload_PropertyValue_IntValue)
		if !ok {
			return 0
		}
		return int8(x.IntValue)
	case Int16:
		x, ok := value.(*Payload_PropertyValue_IntValue)
		if !ok {
			return 0
		}
		return int16(x.IntValue)
	case Int32:
		x, ok := value.(*Payload_PropertyValue_IntValue)
		if !ok {
			return 0
		}
		return int32(x.IntValue)
	case Int64:
		x, ok := value.(*Payload_PropertyValue_LongValue)
		if !ok {
			return 0
		}
		return int64(x.LongValue)
	case UInt8:
		x, ok := value.(*Payload_PropertyValue_IntValue)
		if !ok {
			return 0
		}
		return uint8(x.IntValue)
	case UInt16:
		x, ok := value.(*Payload_PropertyValue_IntValue)
		if !ok {
			return 0
		}
		return uint16(x.IntValue)
	case UInt32:
		x, ok := value.(*Payload_PropertyValue_IntValue)
		if !ok {
			return 0
		}
		return uint32(x.IntValue)
	case UInt64:
		x, ok := value.(*Payload_PropertyValue_LongValue)
		if !ok {
			return 0
		}
		return uint64(x.LongValue)
	case Float:
		x, ok := value.(*Payload_PropertyValue_FloatValue)
		if !ok {
			return 0
		}
		return float32(x.FloatValue)
	case Double:
		x, ok := value.(*Payload_PropertyValue_DoubleValue)
		if !ok {
			return 0
		}
		return float64(x.DoubleValue)
	case String, UUID, Text:
		x, ok := value.(*Payload_PropertyValue_StringValue)
		if !ok {
			return ""
		}
		return string(x.StringValue)
	case Boolean:
		x, ok := value.(*Payload_PropertyValue_BooleanValue)
		if !ok {
			return false
		}
		return bool(x.BooleanValue)
	case DateTime:
		x, ok := value.(*Payload_PropertyValue_LongValue)
		if !ok {
			return time.Unix(0, 0)
		}
		return time.Unix(int64(x.LongValue/1000), int64((x.LongValue%1000)*1000000))
	}
	return 0
}

// HasTemplateValue returns true if the value is a template
func (m *Payload_Metric) HasTemplateValue() bool {
	return m.GetDatatype() == Template
}

// Unmarshal converts a Value to an interface{}
func (m *Payload_Metric) Unmarshal(out interface{}) error {
	mvalue := m.GetValue()
	mdatatype := m.GetDatatype()
	v, ok := out.(reflect.Value)
	if !ok {
		v = reflect.ValueOf(out)
	}
	if v.Kind() == reflect.Ptr && !v.IsNil() {
		v = v.Elem()
	}

	switch mdatatype {
	case Int8:
		x, ok := mvalue.(*Payload_Metric_IntValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Int8 {
			return fmt.Errorf("Cannot convert %v into int8", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(int8(x.IntValue)))
		return nil
	case Int16:
		x, ok := mvalue.(*Payload_Metric_IntValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Int16 {
			return fmt.Errorf("Cannot convert %v into int16", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(int16(x.IntValue)))
		return nil
	case Int32:
		x, ok := mvalue.(*Payload_Metric_IntValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Int32 {
			return fmt.Errorf("Cannot convert %v into int32", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(int32(x.IntValue)))
		return nil
	case Int64:
		x, ok := mvalue.(*Payload_Metric_LongValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Int64 {
			return fmt.Errorf("Cannot convert %v into int64", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(int64(x.LongValue)))
		return nil
	case UInt8:
		x, ok := mvalue.(*Payload_Metric_IntValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Uint8 {
			return fmt.Errorf("Cannot convert %v into uint8", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(uint8(x.IntValue)))
		return nil
	case UInt16:
		x, ok := mvalue.(*Payload_Metric_IntValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Uint16 {
			return fmt.Errorf("Cannot convert %v into uint16", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(uint16(x.IntValue)))
		return nil
	case UInt32:
		x, ok := mvalue.(*Payload_Metric_IntValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Uint32 {
			return fmt.Errorf("Cannot convert %v into uint32", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(uint32(x.IntValue)))
		return nil
	case UInt64:
		x, ok := mvalue.(*Payload_Metric_LongValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Uint64 {
			return fmt.Errorf("Cannot convert %v into uint64", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(uint8(x.LongValue)))
		return nil
	case Float:
		x, ok := mvalue.(*Payload_Metric_FloatValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Float32 {
			return fmt.Errorf("Cannot convert %v into float64", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(float32(x.FloatValue)))
		return nil
	case Double:
		x, ok := mvalue.(*Payload_Metric_DoubleValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Float64 {
			return fmt.Errorf("Cannot convert %v into float64", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(float64(x.DoubleValue)))
		return nil
	case String, UUID, Text:
		x, ok := mvalue.(*Payload_Metric_StringValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.String {
			return fmt.Errorf("Cannot convert %v into string", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(string(x.StringValue)))
		return nil
	case Boolean:
		x, ok := mvalue.(*Payload_Metric_BooleanValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Bool {
			return fmt.Errorf("Cannot convert %v into bool", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		v.Set(reflect.ValueOf(bool(x.BooleanValue)))
		return nil
	case DateTime:
		x, ok := mvalue.(*Payload_Metric_LongValue)
		if !ok {
			return nil
		}
		if v.Kind() != reflect.Struct {
			return fmt.Errorf("Cannot convert %v into time.Time", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		t := time.Unix(int64(x.LongValue/1000), int64((x.LongValue%1000)*1000000))
		v.Set(reflect.ValueOf(t))
		return nil
	case Template:
		ptv, ok := mvalue.(*Payload_Metric_TemplateValue)
		if !ok {
			return nil
		}
		if !ok {
			return fmt.Errorf("Cannot unmarshal into struct")
		}
		if v.Kind() != reflect.Struct {
			return fmt.Errorf("Cannot convert %v into", v.Kind())
		}
		if !v.CanSet() {
			return fmt.Errorf("Cannot assign to %v", v.Kind())
		}
		pt := ptv.TemplateValue
		lookup := make(map[string]*Payload_Metric)
		for _, m := range pt.Metrics {
			if m != nil {
				lookup[*m.Name] = m
			}
		}

		t := v.Type()
		for i := 0; i < v.NumField(); i++ {
			name := t.Field(i).Name
			value, found := lookup[name]
			if found {
				err := value.Unmarshal(v.Field(i))
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}
