package tahu

import (
	"fmt"
	"testing"
	"time"

	"gitlab.sandia.gov/iontraps/GoIonControl/quantity"
)

func TestReflect(t *testing.T) {
	pm := Payload_Metric{}
	q := quantity.Q{M: 3.1415, U: "Peter"}
	err := pm.addValue(q)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	r := quantity.Q{}
	err = pm.Unmarshal(&r)
	if err != nil {
		t.Errorf("Error unmarshalling: %v", err)
	}
	if q != r {
		t.Errorf("Expected %+v received %+v", q, r)
	}
}

func TestReflect2(t *testing.T) {
	sp := MakeSparkplug()
	p := sp.MakePayload()
	q := quantity.Q{M: 3.1415, U: "Peter"}
	p.AddMetric("MyMetric", q)
	m := p.GetMetrics()[0]
	r := quantity.Q{}
	err := m.Unmarshal(&r)
	if err != nil {
		t.Errorf("Error unmarshalling: %v", err)
	}
	if q != r {
		t.Errorf("Expected %+v received %+v", q, r)
	}
}

func TestReflectFloat(t *testing.T) {
	pm := Payload_Metric{}
	o := float64(3.1415)
	err := pm.addValue(o)
	if err != nil {
		t.Errorf("Error adding value: %v\n", err)
	}
	f := float64(0)
	pm.Unmarshal(&f)
	if f != o {
		t.Errorf("Expected %v received %v", o, f)
	}
}

func TestReflectTime(t *testing.T) {
	pm := Payload_Metric{}
	d, _ := time.ParseDuration("1ms")
	o := time.Now().Round(d)
	err := pm.addValue(o)
	if err != nil {
		t.Errorf("Error adding value: %v\n", err)
	}
	f := time.Time{}
	pm.Unmarshal(&f)
	if f != o {
		t.Errorf("Expected %v received %v", o, f)
	}
}

func TestReflectBool(t *testing.T) {
	pm := Payload_Metric{}
	o := true
	err := pm.addValue(o)
	if err != nil {
		t.Errorf("Error adding value: %v\n", err)
	}
	f := false
	pm.Unmarshal(&f)
	if f != o {
		t.Errorf("Expected %v received %v", o, f)
	}
}

func TestTemplateDefinition(t *testing.T) {
	sp := MakeSparkplug()
	p := sp.MakePayload()
	q := quantity.Q{M: 3.1415, U: "Peter"}
	p.AddMetric("_type_/quantity.Q", q)

}
