package tahu

import "time"

// Metric struct models a simple metric without MetaData or Properties
type Metric struct {
	Name         string
	Timestamp    time.Time
	IsHistorical bool
	IsTransient  bool
	IsNull       bool
	Value        interface{}
	Properties   map[string]interface{}
}

// Equal compares two FieldData structs ignoring Properties
func (m *Metric) Equal(g Metric) bool {
	return (m.Value == g.Value && m.Timestamp == g.Timestamp && m.IsHistorical == g.IsHistorical && m.IsTransient == g.IsTransient &&
		m.IsNull == g.IsNull && m.Name == g.Name)
}

// MetricMap has a map of metrics
type MetricMap map[string]Metric

// Update the metrics in the map
func (metricmap MetricMap) Update(metric ...Metric) {
	for _, m := range metric {
		metricmap[m.Name] = m
	}
}

// UpdatePtr the metrics in the map from pointers
func (metricmap MetricMap) UpdatePtr(metric ...*Metric) {
	for _, m := range metric {
		if m != nil {
			metricmap[m.Name] = *m
		}
	}
}

// UpdateMap the metrics in the map from pointers
func (metricmap MetricMap) UpdateMap(othermap MetricMap) {
	for k, v := range othermap {
		metricmap[k] = v
	}
}
