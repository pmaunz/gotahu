package tahu

import (
	"fmt"
	"testing"

	proto "github.com/golang/protobuf/proto"
)

func TestPayload(t *testing.T) {
	sp := MakeSparkplug()
	payload, _ := sp.GetNodeLifePayloads(42)
	fmt.Println(payload.String())
	bytearray, _ := proto.Marshal(payload)
	fmt.Println(bytearray)
}

func TestPayloadSize(t *testing.T) {
	sp := MakeSparkplug()
	payload := sp.GetDdataPayload()

	// Add some random data to the inputs
	payload.AddMetric("", "This is the new data")

	ps, _ := MakePropertySet(map[string]interface{}{"Quality": 500, "Serial": "0123"})

	// Note this data we're setting to STALE via the propertyset as an example
	metric, _ := payload.AddMetric("", true) // random.choice([True, False])
	//metric.AddProperties(map[string]interface{}{"Quality": 500, "Serial": "0123"})
	metric.Properties = ps

	// Note this data we're setting to STALE via the propertyset as an example
	metric, _ = payload.AddMetric("", true) // random.choice([True, False])
	//metric.AddProperties(map[string]interface{}{"Quality": 500, "Serial": "0123"})
	metric.Properties = ps

	// Publish a message data
	bytearray, _ := proto.Marshal(payload)
	fmt.Printf("Payload size: %d\n", len(bytearray))
}

func TestProperties(t *testing.T) {
	properties := map[string]interface{}{"Unit": "ms", "Writable": false}
	sp := MakeSparkplug()
	payload := sp.GetDdataPayload()
	m, _ := payload.AddMetric("mymetric", 42)
	m.AddProperties(properties)
	fmt.Println("TestProperties")
	fmt.Printf("%+v\n", m)
	fmt.Printf("%+v\n", payload)
	pm := m.GetPropertiesMap()
	fmt.Printf("PropertiesMap %+v\n", pm)
}
