package main

import (
	"log"
	"time"

	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/golang/protobuf/proto"
	tahu "gitlab.sandia.gov/iontraps/gotahu"
)

// Application Variables
const (
	serverURL     = "192.168.88.88"
	myGroupID     = "Sparkplug_B_Devices"
	myNodeName    = "Go_Edge_Node"
	myDeviceName  = "Emulated_Device"
	publishPeriod = 5000
	myUsername    = "admin"
	myPassword    = "changeme"
)

// Aliases
const (
	NextServer    = 1
	Rebirth       = 2
	Reboot        = 3
	Dataset       = 4
	NodeMetric0   = 5
	NodeMetric1   = 6
	NodeMetric2   = 7
	NodeMetric3   = 8
	DeviceMetric0 = 9
	DeviceMetric1 = 10
	DeviceMetric2 = 11
	DeviceMetric3 = 12
	MyCustomMotor = 13
)

func makeOnConnect(sp *tahu.Sparkplug, onMessage MQTT.MessageHandler) MQTT.OnConnectHandler {
	// onConnect The callback for when the client receives a CONNACK response from the server.
	return func(client MQTT.Client) {
		log.Println("Connected")
		// Subscribing in onconnect() means that if we lose the connection and
		// reconnect then subscriptions will be renewed.
		client.Subscribe(tahu.MakeTopic(myGroupID, "NCMD", myNodeName, "+"), 0, onMessage)
		client.Subscribe(tahu.MakeTopic(myGroupID, "DCMD", myNodeName, "+"), 0, onMessage)
	}
}

func makeOnMessage(sp *tahu.Sparkplug) MQTT.MessageHandler {
	// onMessage The callback for when a PUBLISH message is received from the server
	return func(client MQTT.Client, msg MQTT.Message) {
		ns, err := tahu.ParseNamespace(msg.Topic())
		log.Printf("Message arrived: Topic: %s Namespace: %+v\n", msg.Topic(), ns)
		if err != nil {
			return
		}
		if ns.GroupID == myGroupID &&
			ns.MessageType[1:] == "CMD" &&
			ns.NodeID == myNodeName {
			inboundPayload, err := sp.UnmarshalPayload(ns, msg.Payload())
			if err != nil {
				log.Printf("Payload is not sparkplug compatible: %s\n", err)
			}
			for _, metric := range inboundPayload.GetMetrics() {
				name := metric.GetName()
				alias := metric.GetAlias()
				log.Printf("Name: '%s', Alias: %d\n", name, alias)
				if name == "Node Control/Next Server" || alias == NextServer {
					// 'Node Control/Next Server' is an NCMD used to tell the device/client application to
					// disconnect from the current MQTT server and connect to the next MQTT server in the
					// list of available servers.  This is used for clients that have a pool of MQTT servers
					// to connect to.
					log.Println("'Node Control/Next Server' is not implemented in this example")
				} else if name == "Node Control/Rebirth" || alias == Rebirth {
					// 'Node Control/Rebirth' is an NCMD used to tell the device/client application to resend
					// its full NBIRTH and DBIRTH again.  MQTT Engine will send this NCMD to a device/client
					// application if it receives an NDATA or DDATA with a metric that was not published in the
					// original NBIRTH or DBIRTH.  This is why the application must send all known metrics in
					// its original NBIRTH and DBIRTH messages.
					publishNodeBirth(client, sp)
				} else if name == "Device Control/Rebirth" || alias == Rebirth {
					// 'Node Control/Rebirth' is an NCMD used to tell the device/client application to resend
					// its full NBIRTH and DBIRTH again.  MQTT Engine will send this NCMD to a device/client
					// application if it receives an NDATA or DDATA with a metric that was not published in the
					// original NBIRTH or DBIRTH.  This is why the application must send all known metrics in
					// its original NBIRTH and DBIRTH messages.
					publishDeviceBirth(client, sp)
				} else if name == "Node Control/Reboot" || alias == Reboot {
					// 'Node Control/Reboot' is an NCMD used to tell a device/client application to reboot
					// This can be used for devices that need a full application reset via a soft reboot.
					// In this case, we fake a full reboot with a republishing of the NBIRTH and DBIRTH
					// messages.
					publishBirth(client, sp)
				} else if name == "output/Device Metric2" || alias == DeviceMetric2 {
					// This is a metric we declared in our DBIRTH message and we're emulating an output.
					// So, on incoming 'writes' to the output we must publish a DDATA with the new output
					// value.  If this were a real output we'd write to the output and then read it back
					// before publishing a DDATA message.

					// We know this is an Int16 because of how we declated it in the DBIRTH
					newValue := metric.Value
					log.Printf("CMD message for output/Device Metric2 - New Value: %v\n", newValue)

					// Create the DDATA payload - Use the alias because this isn't the DBIRTH
					payload := sp.GetDdataPayload()
					payload.AddMetric("", newValue)

					// Publish a message data
					//byteArray, _ := proto.Marshal(payload)
					//client.Publish("spBv1.0/"+myGroupID+"/DDATA/"+myNodeName+"/"+myDeviceName, 0, false, byteArray)
				} else if name == "output/Device Metric3" || alias == DeviceMetric3 {
					// This is a metric we declared in our DBIRTH message and we're emulating an output.
					// So, on incoming 'writes' to the output we must publish a DDATA with the new output
					// value.  If this were a real output we'd write to the output and then read it back
					// before publishing a DDATA message.

					// We know this is an Boolean because of how we declated it in the DBIRTH
					newValue := metric.Value
					log.Printf("CMD message for output/Device Metric3 - New Value: %v\n", newValue)

					// Create the DDATA payload - use the alias because this isn't the DBIRTH
					payload := sp.GetDdataPayload()
					payload.AddMetric("", newValue)

					// Publish a message data
					//byteArray, _ := proto.Marshal(payload)
					//client.Publish("spBv1.0/"+myGroupID+"/DDATA/"+myNodeName+"/"+myDeviceName, 0, false, byteArray)
				} else {
					log.Printf("Unknown command: %s\n", *metric.Name)
				}
			}
		} else {
			log.Println("Unknown command...")
		}
		log.Println("Done publishing")
	}
}

func publishBirth(client MQTT.Client, sparkplug *tahu.Sparkplug) {
	publishNodeBirth(client, sparkplug)
	publishDeviceBirth(client, sparkplug)
}

// publishNodeBirth Publish the NBIRTH certificate
func publishNodeBirth(client MQTT.Client, sparkplug *tahu.Sparkplug) {
	log.Println("Publishing Node Birth")

	// Create the node birth payload
	payload, _ := sparkplug.GetNodeLifePayloads(0)

	//  Set up the Node Controls
	payload.AddMetric("Node Control/Next Server", false)
	payload.AddMetric("Node Control/Rebirth", false)
	payload.AddMetric("Node Control/Reboot", false)

	// Add some regular node metrics
	payload.AddMetric("Property/Device", "Sample Device")
	payload.AddMetric("Node Metric1", true)
	payload.AddNullMetric("Node Metric3", tahu.Int32)

	// Create a DataSet (012 - 345) two rows with Int8, Int16, and Int32 contents and headers Int8s, Int16s, Int32s and add it to the payload
	columns := []string{"Int8s", "Int16s", "Int32s"}
	types := []uint32{tahu.Int8, tahu.Int16, tahu.Int32}
	dataset := payload.InitDatasetMetric("DataSet", Dataset, columns, types)
	dataset.AddRow([]interface{}{0, 1, 2})
	dataset.AddRow([]interface{}{3, 4, 5})

	// Add a metric with a custom property
	metric, _ := payload.AddMetric("Node Metric2", 13)
	props := map[string]interface{}{"engUnit": "MyCustomUnits"}
	metric.AddProperties(props)
	// Create the UDT definition value which includes two UDT members and a single parameter and add it to the payload
	template := payload.InitTemplateMetric("types/CustomMotor", 0, "") // No alias for Template definitions
	template.AddParameter("Index", "0")
	template.AddMetric("RPMs", 0) // No alias in UDT members
	template.AddMetric("AMPs", 0) // No alias in UDT members

	// Publish the node birth certificate
	bytearray, _ := proto.Marshal(payload)
	client.Publish("spBv1.0/"+myGroupID+"/NBIRTH/"+myNodeName, 0, false, bytearray)
}

// publishDeviceBirth Publish the DBIRTH certificate
func publishDeviceBirth(client MQTT.Client, sparkplug *tahu.Sparkplug) {
	log.Println("Publishing Device Birth")
	// Get the payload
	payload := sparkplug.GetDeviceBirthPayload(0)

	// Add some device metrics
	payload.AddMetric("Input/Device Metric0", "hello device")
	payload.AddMetric("Input/Device Metric1", 12)
	payload.AddMetric("Property/Model", "Emulated Device")
	payload.AddMetric("Property/Serial", 123)
	payload.AddMetric("output/Device Metric2", 16)
	payload.AddMetric("output/Device Metric3", true)

	// Create the UDT definition value which includes two UDT members and a single parameter and add it to the payload
	template := payload.InitTemplateMetric("MyCustomMotor", MyCustomMotor, "CustomMotor")
	template.AddParameter("Index", "1")
	template.AddMetric("RPMs", 123) // No alias in UDT members
	template.AddMetric("AMPs", 456) // No alias in UDT members

	// Publish the initial data with the Device BIRTH certificate
	bytearray, _ := proto.Marshal(payload)
	client.Publish("spBv1.0/"+myGroupID+"/DBIRTH/"+myNodeName+"/"+myDeviceName, 0, false, bytearray)
}

func onConnectionLost(client MQTT.Client, err error) {
	log.Printf("Connection Lost: %v\n", err)
}

func main() {
	log.Println("Starting main application")

	// Create the node death payload
	sparkplug := tahu.MakeSparkplug()
	_, deathPayload := sparkplug.GetNodeLifePayloads(0)
	onMessage := makeOnMessage(sparkplug)
	onConnect := makeOnConnect(sparkplug, onMessage)

	// Start of main program - Set up the MQTT client connection
	opts := MQTT.NewClientOptions().AddBroker("tcp://192.168.88.88:1883")
	opts.SetClientID("sparkplug")
	opts.SetDefaultPublishHandler(onMessage)
	opts.SetOnConnectHandler(onConnect)
	opts.SetConnectionLostHandler(onConnectionLost)
	opts.SetUsername(myUsername)
	opts.SetPassword(myPassword)
	deathByteArray, _ := proto.Marshal(deathPayload)
	opts.SetBinaryWill("spBv1.0/"+myGroupID+"/NDEATH/"+myNodeName, deathByteArray, 0, false)

	client := MQTT.NewClient(opts)
	token := client.Connect()
	token.Wait()

	// Short delay to allow connect callback to occur
	delay, _ := time.ParseDuration("0.1s")
	time.Sleep(delay)
	// client.loop()

	// Publish the birth certificates
	publishBirth(client, sparkplug)
	i := 1
	for {
		// Periodically publish some new data
		payload := sparkplug.GetDdataPayload()

		// Add some random data to the inputs
		payload.AddMetric("", "This is the new data")

		// Note this data we're setting to STALE via the propertyset as an example
		metric, _ := payload.AddMetric("", i) // random.choice([True, False])
		metric.AddProperties(map[string]interface{}{"Quality": 500})

		// Publish a message data
		bytearray, _ := proto.Marshal(payload)
		client.Publish("spBv1.0/"+myGroupID+"/DDATA/"+myNodeName+"/"+myDeviceName, 0, false, bytearray)

		// Sit and wait for inbound or outbound events
		delay, _ := time.ParseDuration("5s")
		time.Sleep(delay)
		i++
	}
}
